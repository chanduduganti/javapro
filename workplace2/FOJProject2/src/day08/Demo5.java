package day08;

public class Demo5 {

	public static int greatestOf2(int num1, int num2){
		return (num1 > num2) ? num1 : num2;
	}

	public static int greatestOf3(int number1, int number2, int number3 ){
                                              //true statement                               // false statement
		return (number1 > number2) ? ((number1 > number3) ? number1 : number3) : ((number2 > number3) ? number2 : number3);
	}

	public static void main(String[] args) {

		System.out.print(greatestOf2(2, 3)); //3
		System.out.print(greatestOf2(2, 1)); //2
		System.out.print(greatestOf2(4, 8)); //8
		
		System.out.println("\n");

		System.out.print(greatestOf3(2, 3, 4)); //4
		System.out.print(greatestOf3(5, 6, 1)); //6
		System.out.print(greatestOf3(2, 1, 5)); //5
	}

}
