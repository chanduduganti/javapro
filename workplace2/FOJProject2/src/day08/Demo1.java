

package day08;
//passing arguments in array
public class Demo1 {

	public static void showArray(int arr[]){
		for(int i = 0; i < arr.length; i++){
			System.out.println( arr[i] +" ");
		}
	}


	public static void main(String[] args) {

		int arr[] = {30, 40, 50, 60, 70};
		showArray(arr);

	}

}
