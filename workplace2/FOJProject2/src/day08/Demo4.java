package day08;

public class Demo4 {

	public static int powerOfTwo(int num, int pow){

		int result = 1;

		for(int i = 0; i < pow; i++){
			result *= num;
		}

		return result;
	}

	public static void main(String[] args) {

		System.out.println(powerOfTwo(2, 3)); //8
		System.out.println(powerOfTwo(3, 4)); //81
		System.out.println(powerOfTwo(4, 5)); //1024
		System.out.println(powerOfTwo(2, 5)); //32


	}

}
