package day08;

public class Demo2 {

	public static int[] sortAscending(int arr[]){
		int temp = 0;
		//Bubble Sort
		for( int i = 0; i < arr.length - 1; i++){
			for( int j = 0; j < arr.length - 1; j++){
				if(arr[j] > arr[j + 1]){
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}

		return arr;
	}

	public static void main(String[] args) {
		int arr[] = {30, 10, 20, 40, 50};

		System.out.print("Before Sorting : ");

		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + " ");
		}

		System.out.println("\n");

		arr = sortAscending(arr);

		System.out.print("After Sorting : ");

		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + " ");
		}

	}

}
