package day08;

public class Demo3 {

	public static int searchArray(int arr[], int num){

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == num){
				return i;
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		int arr[] = {30, 10, 50, 20, 40};

		System.out.println(searchArray(arr, 99));  //-1
		System.out.println(searchArray(arr, 10));  //1
		System.out.println(searchArray(arr, 20));  //3
		System.out.println(searchArray(arr, 30));  //0
		System.out.println(searchArray(arr, 40));  //4

	}

}


