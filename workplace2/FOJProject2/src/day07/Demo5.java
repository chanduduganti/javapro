package day07;

public class Demo5 {

    public static void main(String[] args) {
        // descending order
        int arr[] = {10, 20, 30, 40, 50};
        int arrLength = arr.length;

        for (int i = 0; i < arrLength - 1; i++) {
            for (int j = 0; j < arrLength - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    // swap
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }

        // Print the sorted array
        for (int i = 0; i < arrLength; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
