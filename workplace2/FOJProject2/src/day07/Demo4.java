package day07;

public class Demo4 {
	

    public static void main(String[] args) {
        // ascending order
        int arr[] = {50,40,30,20,10};
        int arrlength = arr.length;
        
        for (int i = 0; i < arrlength - 1; i++) {
            for (int j = 0; j < arrlength - i - 1; j++) {
                if (arr[j] > arr[j+1]) {
                    // swap
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        
        // Print the sorted array
        for (int i = 0; i < arrlength; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
