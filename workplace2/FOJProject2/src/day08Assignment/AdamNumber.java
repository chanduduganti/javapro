package day08Assignment;
import java.util.Scanner;

public class AdamNumber {

	// Function to reverse a number
	public static int reverseNumber(int num) {
		int rev = 0;
		while (num > 0) {
			rev = rev * 10 + num % 10;
			num = num / 10;
		}
		return rev;
	}

	// Function to check if a number is Adam's number or not
	public static boolean isAdamsNumber(int num) {
		int square = num * num;
		int revNum = reverseNumber(num);
		int revSquare = reverseNumber(square);
		int revNumSquare = revNum * revNum;
		return revNumSquare == revSquare;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int number = scanner.nextInt();

		if (isAdamsNumber(number)) {
			System.out.println(number + " is an Adam's number.");
		} else {
			System.out.println(number + " is not an Adam's number.");
		}
		scanner.close();
	}
}
