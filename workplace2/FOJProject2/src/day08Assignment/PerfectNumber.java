package day08Assignment;

public class PerfectNumber {

	public static boolean PerfectNumber(int number){

		int sum = 0;
		for(int i = 1 ; i <= number / 2; i++){
			if(number % i == 0){
				sum+=i;
			}

		}
		return sum == number;
	}

	public static void main(String[] args) {
		int number = 6;
		if(PerfectNumber(number)){
			System.out.println(number + " is Perfect Number");
		}else{
			System.out.println(number + " is not perfect number");
		
		}

	}
}
