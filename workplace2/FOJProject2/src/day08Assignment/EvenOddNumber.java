package day08Assignment;

public class EvenOddNumber {

	public static void main(String[] args) {
		


		int start = 1;
		int end = 25;

		int evenSum = 0;
		int oddSum = 0;

		for (int i = start; i <= end; i++) {
			if (i % 2 == 0) {
				evenSum += i;
			} else {
				oddSum += i;
			}
		}

		System.out.println("Sum of even numbers from " + start + " to " + end + " : " + evenSum);
		System.out.println("Sum of odd  numbers from " + start + " to " + end + " : " + oddSum);
	}




}
