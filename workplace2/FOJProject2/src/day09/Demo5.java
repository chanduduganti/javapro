package day09;

//Two Dimensional Arrays
public class Demo5 {
	
	public static int diagonalSum(int arr[][]) {					
		int sum = 0;
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (i == j) {
					sum += arr[i][j];
				}
			}
		}
		
		return sum;
	}
	
	public static void main(String[] args) {		
		int arr1[][] = new int[][]{{10, 20, 30}, {40, 51, 60}, {70, 80, 91}};				
		int arr2[][] = new int[][]{{11, 12, 13}, {14, 15, 16}, {17, 18, 19}};						
		int arr3[][] = new int[][]{{21, 20, 30}, {30, 41, 50}, {60, 70, 81}};
				
		System.out.println(diagonalSum(arr1));	//152
		System.out.println(diagonalSum(arr2));	//45
		System.out.println(diagonalSum(arr3));	//143
	}
}

