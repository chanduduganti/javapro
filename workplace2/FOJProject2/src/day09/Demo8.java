package day09;

public class Demo8 {
	public static int FindSecondLargest(int arr[]){
		int largest = Integer.MIN_VALUE;
		int secondLargest = Integer.MIN_VALUE;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > largest) {
				secondLargest = largest;
				largest = arr[i];
			} else if (arr[i] > secondLargest && arr[i] != largest) {
				secondLargest = arr[i];
			}
		}

		return secondLargest;
	}

	public static void main(String[] args){
		int arr1[] = {10, 20, 30, 40, 50};
		int arr2[] = {83, 96, 125, 183, 458, 329};
		int arr3[] = {430, 480, 350};
		int arr4[] = {480, 350};

		System.out.println(FindSecondLargest(arr1)); //40
		System.out.println(FindSecondLargest(arr2)); //329
		System.out.println(FindSecondLargest(arr3)); //430
		System.out.println(FindSecondLargest(arr4)); //350
	}

}
