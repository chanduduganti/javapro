package oops;
//if local and instance variables names are same

class Sample{
	private int a,b;//instance variable

	public Sample(){
		System.out.println("Hi! This is Constructor...");
	}
	public Sample(int a,int b){
		System.out.println("Hi! This is Parameterized..");
		this.a = a;
		this.b = b;
	}
	public void read(int a ,int b){
		this.a = a;
		this.b = b;
	}
	public void show(){
		System.out.println(a);
		System.out.println(b);
	}
	public void sum(){
		System.out.println("The Sum is : " + (a + b));
	}
	public void mul(){
		System.out.println("The Mul is : " + ( a * b));
	}
}

public class ClassDemo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Sample obj1 = new Sample();
		obj1.read(10,20);
		obj1.show();
		obj1.sum();

		Sample obj2 = new Sample(100, 200);
		obj2.show();
		obj2.sum();
		
		Sample obj3 = new Sample(23,45);
		obj3.show();
		obj3.mul();


	}

}


