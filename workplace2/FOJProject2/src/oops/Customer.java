package oops;

public class Customer {
	
	private int id;
		private String firstname;
		private String lastname;
		private String address;
		private double balance;

public Customer(){
	
}
public Customer(int id, String firstname,String lastname,String address,
		double balance){
	super();
	this.id = id;
	this.firstname = firstname ;
	this.lastname = lastname;
	this.address  = address;
	this.balance = balance;
	
}


	

	


	public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public double getBalance() {
	return balance;
}


@Override
public String toString() {
	return "Customer [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", address=" + address
			+ ", balance=" + balance + "]";
}
public void setBalance(double balance) {
	this.balance = balance;
}
	

}
