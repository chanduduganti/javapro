package oops;

public class Student {
	
	private int id;
	private String firstname;
	private String lastname;
	private double fees;
	private String coursename;
	private int batchnumber;
	public Student(int id, String firstname, String lastname, double fees, String coursename, int batchnumber) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.fees = fees;
		this.coursename = coursename;
		this.batchnumber = batchnumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public double getFees() {
		return fees;
	}
	public void setFees(double fees) {
		this.fees = fees;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public int getBatchnumber() {
		return batchnumber;
	}
	public void setBatchnumber(int batchnumber) {
		this.batchnumber = batchnumber;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", fees=" + fees
				+ ", coursename=" + coursename + ", batchnumber=" + batchnumber + "]";
	}
	
	
	

}
