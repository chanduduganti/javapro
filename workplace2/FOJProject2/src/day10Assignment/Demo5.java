package day10Assignment;
public class Demo5 {
    public static void main(String[] args) {
        String inputString = "Talentsprint!";
        int vowelCount = countVowels(inputString);
        System.out.println("Number of vowels: " + vowelCount);
    }
    
    public static int countVowels(String str) {
        // Define a set of vowels
        String vowels = "aeiouAEIOU";
        
        int count = 0;
        // Iterate through each character in the string
        for (int i = 0; i < str.length(); i++) {
            // If the character is a vowel, increment the count
            if (vowels.contains(String.valueOf(str.charAt(i)))) {
                count++;
            }
        }
        return count;
    }
}
