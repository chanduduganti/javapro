package day10Assignment;

public class NonDuplicate {

	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 1, 2, 4, 6};

		// Print unique elements using nested loops
		System.out.print("NonDuplicate elements: ");
		for (int i = 0; i < arr.length; i++) {
			boolean isNonDuplicate = true;
			for (int j = 0; j < i; j++) {
				if (arr[i] == arr[j]) {
					isNonDuplicate = false;
					break;
				}
			}
			if (isNonDuplicate) {
				System.out.print(arr[i] + " ");
			}
		}
	}

}
