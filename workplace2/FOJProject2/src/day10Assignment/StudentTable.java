package day10Assignment;



import java.util.Scanner;

public class StudentTable {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int studentId = 100;

    while (true) {
      System.out.print("Enter name (exit to quit): ");
      String name = scanner.nextLine();
      if (name.equalsIgnoreCase("exit")) {
        break;
      }

      int[] marks = new int[3];
      System.out.print("Enter marks for 3 subjects (separated by space): ");
      String line = scanner.nextLine();
      String[] tokens = line.split(" ");

      for (int i = 0; i < 3; i++) {
        marks[i] = Integer.parseInt(tokens[i]);
      }

      int total = 0;
      for (int mark : marks) {
        total += mark;
      }
      double average = (double) total / marks.length;

      String result = average >= 35 && marks[0] >= 35 && marks[1] >= 35 && marks[2] >= 35 ? "Pass" : "Fail";
      String division = average >= 90 ? "I" : average >= 80 ? "II" : average >= 70 ? "III" : "";

      System.out.printf("%d\t%s\t%d\t%.2f\t%s\t%s\n", ++studentId, name, total, average, result, division);
    }

    scanner.close();
  }
}
