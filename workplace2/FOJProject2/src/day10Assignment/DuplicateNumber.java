package day10Assignment;

public class DuplicateNumber {

	public static void main(String[] args) {
		int[] data = {1, 2, 3, 1, 2, 4, 6};
		findDuplicates(data);
	}

	public static void findDuplicates(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			int count = 0;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					count++;
				}
			}
			if (count > 0) {
				boolean printed = false;
				for (int k = 0; k < i; k++) {
					if (arr[i] == arr[k]) {
						printed = true;
						break;
					}
				}
				if (!printed) {
					System.out.println(arr[i] + " : " + (count + 1));
				}
			}
		}
	}
}
