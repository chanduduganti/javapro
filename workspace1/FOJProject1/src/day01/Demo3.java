package day01;
import java.util.Scanner;


public class Demo3 {

	public static void main(String[] args) {

		int a;
		double b;
		boolean c;
		String d;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter int value:");
		a = scan.nextInt();
		System.out.println("Enter Double value:");
		b = scan.nextDouble();
		System.out.println("Enter Boolean value:");
		c = scan.nextBoolean();
		System.out.println("Enter String value:");
		d = scan.next();
		System.out.println("a = "+a+"\nb = "+b);
		System.out.println("c = "+c+"\nd = "+d);


	}

}
