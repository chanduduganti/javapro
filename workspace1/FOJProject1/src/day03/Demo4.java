package day03;

public class Demo4 {
public static void main(String[] args) {
		
		int num1 = 10;
		int num2 = 0;		
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");		
		
		System.out.println("Performing Pre Increment and Decrement");
		System.out.println("--------------------------------------");
		
		num2 = ++num1;
		System.out.println("num2 = ++num1;");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		num2 = --num1;
		System.out.println("num2 = --num1;");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");		

		System.out.println("Performing Post Increment and Decrement");
		System.out.println("---------------------------------------");
		
		num2 = num1++;
		System.out.println("num2 = num1++;");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		num2 = num1--;
		System.out.println("num2 = num1--;");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
	}

	
	

}
