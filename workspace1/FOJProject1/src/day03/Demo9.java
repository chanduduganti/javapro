package day03;


public class Demo9 {
    public static void main(String[] args) {
        int number = 15; // Example number to check
        
        // Check divisible by 3
        if (isDivisibleBy3(number)) {
            System.out.println(number + " is divisible by 3");
        } else {
            System.out.println(number + " is not divisible by 3");
        }
        
        // Check divisible by 5
        if (isDivisibleBy5(number)) {
            System.out.println(number + " is divisible by 5");
        } else {
            System.out.println(number + " is not divisible by 5");
        }
        
        // Check divisible by both 3 and 5
        if (isDivisibleBy3And5(number)) {
            System.out.println(number + " is divisible by both 3 and 5");
        } else {
            System.out.println(number + " is not divisible by both 3 and 5");
        }
    }
    
    // Function to check divisible by 3
    public static boolean isDivisibleBy3(int num) {
        return num % 3 == 0;
    }
    
    // Function to check divisible by 5
    public static boolean isDivisibleBy5(int num) {
        return num % 5 == 0;
    }
    
    // Function to check divisible by both 3 and 5
    public static boolean isDivisibleBy3And5(int num) {
        return isDivisibleBy3(num) && isDivisibleBy5(num);
    }
}

