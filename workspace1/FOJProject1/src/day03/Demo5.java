package day03;

public class Demo5 {
	
	public static int sum(int num1, int num2) {
		return num1 + num2;
	}
	
	public static void main(String[] args) {
		
		int result = sum(10, 3);
		System.out.println("Sum = " + result + "\n");
		
		System.out.println("Sum = " + sum(20, 3));
	}


}
