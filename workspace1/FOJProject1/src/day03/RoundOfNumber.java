package day03;

import java.util.Scanner;
public class RoundOfNumber {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.print("Enter a number to round: ");
	        double number = scanner.nextDouble();

	        double roundedNumberIfElse = roundNumberIfElse(number);
	        System.out.println("Rounded number using if-else: " + roundedNumberIfElse);

	        double roundedNumberFunction = roundNumberFunction(number);
	        System.out.println("Rounded number using function: " + roundedNumberFunction);
	    }

	    // Function to round a number using if-else statements
	    public static double roundNumberIfElse(double num) {
	        double roundedNum;
	        if (num >= 0) {
	            roundedNum = Math.floor(num + 0.5);
	        } else {
	            roundedNum = Math.ceil(num - 0.5);
	        }
	        return roundedNum;
	    }

	    // Function to round a number using built-in Math.round() function
	    public static double roundNumberFunction(double num) {
	        return Math.round(num);
	    }
}
	


