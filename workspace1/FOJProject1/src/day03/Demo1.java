package day03;

//Arithmetic Operators (+, -, *, /, %)
public class Demo1 {
	public static void main(String[] args) {
		
		int num1 = 10;
		int num2 = 3;
		
		System.out.println("Sum = " + (num1 + num2));
		System.out.println("Sub = " + (num1 - num2));
		System.out.println("Mul = " + (num1 * num2));
		System.out.println("Quo = " + (num1 / num2));
		System.out.println("Rem = " + (num1 % num2));
	}
}



