package day03;

public class Demo8 {
    public static void main(String[] args) {
        int maxPalindrome = 999; 
        
        System.out.println("Palindromes up to " + maxPalindrome + ":");
        printPalindromes(1, maxPalindrome);
    }
    
    
    public static void printPalindromes(int start, int max) {
        if (start <= max) {
            if (isPalindrome(start)) {
                System.out.println(start);
            }
            printPalindromes(start + 1, max);
        }
    }
    
    
    public static boolean isPalindrome(int num) {
        int reversed = reverseNumber(num);
        return num == reversed;
    }
    
    // Function to reverse a number
    public static int reverseNumber(int num) {
        int reversed = 0;
        int temp = num;
        while (temp != 0) {
            int digit = temp % 10;
            reversed = reversed * 10 + digit;
            temp /= 10;
        }
        return reversed;
    }
}
