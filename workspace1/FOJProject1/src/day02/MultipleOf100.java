package day02;

import java.util.Scanner;

public class MultipleOf100 {

    // Function to find the next multiple of 100
    public static int findNextMultipleOf100(int num) {
        if (num % 100 == 0) {
            return num; // If num is already a multiple of 100, return num
        } else {
            // Calculate the next multiple of 100
            int nextMultiple = (num / 100 + 1) * 100;
            return nextMultiple;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num;

        // Input a number from the user
        System.out.print("Enter a number: ");
        num = scanner.nextInt();

        // Find the next multiple of 100
        int nextMultipleOf100 = findNextMultipleOf100(num);

        // Display the result
        System.out.println("Next multiple of 100: " + nextMultipleOf100);

        scanner.close();
    }
}

