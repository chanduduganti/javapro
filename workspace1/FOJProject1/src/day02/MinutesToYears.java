package day02;

import java.util.Scanner;
public class MinutesToYears {
	
	  // Function to convert minutes to years
    public static void convertMinutesToYears(int minutes) {
        if (minutes < 0) {
            System.out.println("Invalid input! Minutes must be non-negative.");
            return;
        }
        
        int minutesInYear = 60 * 24 * 365;
        long years = minutes / minutesInYear;
        long remainingMinutes = minutes % minutesInYear;
        
        System.out.println(minutes + " minutes is approximately " + years + " years and " + remainingMinutes + " minutes.");
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int minutes;
        
        System.out.print("Enter the number of minutes: ");
        minutes = scanner.nextInt();
        
        convertMinutesToYears(minutes);
        
        scanner.close();
    }
}


