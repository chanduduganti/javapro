package day02;

public class Demo6 {
	public static void main(String[] args) {
		
		String stringValue = "25";
		byte byteValue = Byte.parseByte(stringValue);		//String to byte
		short shortValue = Short.parseShort(stringValue);	//String to short
		int intValue = Integer.parseInt(stringValue);		//String to int
		long longValue = Long.parseLong(stringValue);		//String to long
		
		//Lower to Higher
		System.out.println("stringValue : " + stringValue);
		System.out.println("byteValue   : " + byteValue);
		System.out.println("shortValue  : " + shortValue);
		System.out.println("intValue    : " + intValue);
		System.out.println("longValue   : " + longValue);
		System.out.println();
		
		longValue  = 45;
		intValue   = (int) longValue;			//Type Casting to Int
		shortValue = (short) intValue;			//Type Casting to Short		
		byteValue  = (byte) shortValue;			//Type Casting to Byte
		
		stringValue  = byteValue + "";			//Converting to String (Any to String)
		
		//Higher to Lower
		System.out.println("longValue   : " + longValue);
		System.out.println("intValue    : " + intValue);
		System.out.println("shortValue  : " + shortValue);
		System.out.println("byteValue   : " + byteValue);
		System.out.println("stringValue : " + stringValue);
		System.out.println();
	}
}

