package day02;

import java.util.Scanner;
public class Demo9 {
	 // Function to calculate the area of a circle
    public static double calculateCircleArea(double radius) {
        return Math.PI * radius * radius;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner scanner = new Scanner(System.in);
	        double radius, area;
	        
	        System.out.print("Enter the radius of the circle: ");
	        radius = scanner.nextDouble();
	        
	        // Check if radius is valid (greater than zero)
	        if (radius > 0) {
	            area = calculateCircleArea(radius);
	            System.out.println("Area of the circle: " + area);
	        } else {
	            System.out.println("Invalid radius! Radius must be greater than zero.");
	        }
	        
	        scanner.close();
	    }

	}


