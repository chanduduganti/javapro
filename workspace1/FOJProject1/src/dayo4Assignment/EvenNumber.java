package dayo4Assignment;

public class EvenNumber {
    
    public static int isEven(int number){
    	
        if(number <= 0)
            return -1;
        else if(number % 2 == 0)
            return 1;
        else
            return 0;
    }

    public static void main(String[] args) {
    	
        System.out.println(isEven(23));
        System.out.println(isEven(26));
        System.out.println(isEven(-5));
        System.out.println(isEven(0));
    }
}