package dayo4;

//Leap Year
public class Demo5 {

	public static boolean isLeapYear(int year) {		
		if ((year % 400 == 0) || (year % 100 != 0 && year % 4 == 0)) 
			return true;
		
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(isLeapYear(1600));	//true
		System.out.println(isLeapYear(1700));	//false
		System.out.println(isLeapYear(1800));	//false
		System.out.println(isLeapYear(1900));	//false
		System.out.println(isLeapYear(2000));	//true
		System.out.println(isLeapYear(1996));	//true
		System.out.println(isLeapYear(2020));	//true		
	}
}

