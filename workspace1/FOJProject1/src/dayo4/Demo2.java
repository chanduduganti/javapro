package dayo4;

//3 DigitPalindrome
public class Demo2 {
	
	public static boolean isPalindrome(int num) {
		return (num % 10) == (num / 100);
	}	
	
	public static void main(String[] args) {
		System.out.println(isPalindrome(121));
		System.out.println(isPalindrome(123));
	}
}

