package dayo4;

//FizzBizz
public class Demo3 {
	
	public static String isDivisibleBy3Or5(int num) {
		String result = "";
		
		if (num % 3 == 0)
			result += "Fizz";
		
		if (num % 5 == 0)
			result += "Bizz";
			
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(isDivisibleBy3Or5(3));
		System.out.println(isDivisibleBy3Or5(5));
		System.out.println(isDivisibleBy3Or5(15));
	}
}

