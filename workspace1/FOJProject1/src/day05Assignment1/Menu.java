package day05Assignment1;
import java.util.Scanner;


public class Menu{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Display the menu
        System.out.println("Juice Menu:");
        System.out.println("1. Carrot Juice");
        System.out.println("2. Beetroot Juice");
        System.out.println("3. Apple Juice");
        System.out.println("Enter the number of the juice you want to know the price for: ");
        
        // Read the user's choice
        
        int choice = scanner.nextInt();
        
        // Display the price based on the user's choice
        switch (choice) {
            case 1:
                System.out.println("Carrot Juice - Price: $3.50");
                break;
            case 2:
                System.out.println("Beetroot Juice - Price: $4.00");
                break;
            case 3:
                System.out.println("Apple Juice - Price: $3.00");
                break;
            default:
                System.out.println("Invalid choice");
                break;
        }
        
        scanner.close();
    }
}
