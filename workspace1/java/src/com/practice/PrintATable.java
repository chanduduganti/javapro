package com.practice;
import java.util.Scanner;

public class PrintATable {
	
	public static void main (String args[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the table");
		int num =sc.nextInt();
		sc.close();
		
		for(int i = 1 ; i<= 10 ; i++){
			System.out.println(num + " * " + i + " = " + (num * i));
		}
		
	}

}
