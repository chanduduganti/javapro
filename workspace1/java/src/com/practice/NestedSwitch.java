package com.practice;

import java.util.Scanner;

public class NestedSwitch {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int empID = in.nextInt();
		String department = in.next();

		switch(empID){
		case 1 :
			System.out.println("Rahul Rana");
			break;
		case 2 :
			System.out.println("chandu");
			break;
		case 3 :
			System.out.println("Emp Number 3");
			switch(department){
			case "IT":
				System.out.println("IT Department");
				break;
			case "Manager":
				System.out.println("Manager Depatment");
				break;
			default :
				System.out.println("enter the default Department");
			}
			break;
		default :
			System.out.println("enter the correct empID");
			
		}


	}
}

