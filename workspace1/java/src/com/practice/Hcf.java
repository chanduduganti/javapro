
package com.practice;

public class Hcf {

    public static void main(String args[]) {
        int a = 98;
        int b = 56;
        int hcf = 1; // Initialize hcf to 1

        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }

        // The HCF is now stored in 'a'
        hcf = a;
        System.out.println("The HCF of 98 and 56 is: " + hcf);
    }
}




