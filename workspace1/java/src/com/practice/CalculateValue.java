package com.practice;
import java.util.Scanner;

//Take in two numbers and an operator (+, -, *, /) and calculate the value. (Use if conditions)
public class CalculateValue {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the operator (+, -, *, /):");
		String operator = sc.next();
		System.out.println("Enter the num1:");
		int num1 = sc.nextInt();
		System.out.println("Enter the num2:");
		int num2 = sc.nextInt();

		// Calculating the result based on the operator
		int result = 0;
		if (operator.equals("+")) {
			result = num1 + num2;
		} else if (operator.equals("-")) {
			result = num1 - num2;
		} else if (operator.equals("*")) {
			result = num1 * num2; // Multiplication calculation
		} else if (operator.equals("/")) {
			if(num2 != 0){
				result = num1 / num2;
			}else{
				System.out.println("cannot divide by zero");
				sc.close();
				return;
			}

		} else {
			System.out.println("Invalid operator");
			return; // Exit the program if the operator is invalid
		}

		System.out.println("The result of " + num1 + " " + operator + " " + num2 + " is: " + result);
		sc.close();
	}



}


