package com.practice;
import java.util.Scanner;

public class SwitchCase {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the fruit name");
		String fruits = sc.next();

		switch(fruits){
		case "Mango" :
			System.out.println("the king of the fruits");
			break;
		case "Apple" :
			System.out.println("the sweet red color fruit");
			break;
		case "banana" :
			System.out.println("the healthy yellow color fruit");
			break;
		case "orange":
			System.out.println("the round fruit");
			break;
		default :
			System.out.println("enter the correct fruit");
		}




	}

}
