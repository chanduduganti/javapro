package com.practice;
import java.util.Scanner;
//Write a program to input principal, time, and rate (P, T, R) from the user and find Simple Interest

public class SimpleInterest {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the principal");
		 double principal = sc.nextInt();
		 
		 System.out.println("Enter the time");
		 double time = sc.nextInt();
		 
		 System.out.println("Enter the rate");
		 double rate = sc.nextInt();
		 
		 double simpleInterest = (principal*time*rate);
		 
		 System.out.println("The SimpleInterest is:" +simpleInterest);

	}

}
