package com.practice;

public class ArmstrongNumber{

	public static void main (String args[]){

		int n = 153;
		int original = n;
		int	Armstrong = 0;
		while(n > 0){
			int rem = n %10;
			rem = (int) Math.pow(rem ,  3);
			Armstrong = Armstrong + rem;
			n = n / 10;

		}
		if(Armstrong == original){
			System.out.println("It is a Armstrong number");
		}else{
			System.out.println("It is not a Armstrong number");
		}
	}
}
