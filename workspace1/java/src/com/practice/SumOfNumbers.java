package  com.practice;
import java.util.Scanner;

public class SumOfNumbers{
	
	public static int sumOfNumbers(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number1");
		int num1 = sc.nextInt();
		System.out.println("Enter the number2");
		int num2 = sc.nextInt();
		
		int sum = num1 + num2;
		System.out.println(sum);
		sc.close();
		return sum;
		
	}
	
	
	public static void main(String args[]){
		sumOfNumbers();
		
	}
}
