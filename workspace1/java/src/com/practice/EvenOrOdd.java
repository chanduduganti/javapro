package com.practice;
import java .util.Scanner;

//Write a program to print whether a number is even or odd, also take input from the user.
public class EvenOrOdd {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the number");
		 int number = sc.nextInt();
		 sc.close();
		 
		 if(number  % 2 == 0){
			 System.out.println("The number is Even");
		}else{
			System.out.println("The number is odd");
		}
		

	}

}
