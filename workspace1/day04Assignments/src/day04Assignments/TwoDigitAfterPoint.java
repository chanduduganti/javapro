package day04Assignments;

import java.text.DecimalFormat;

public class TwoDigitAfterPoint {

	public static void main(String[] args) {
		
		double number = 12.333333333 ;
		DecimalFormat digit = new DecimalFormat("#.##");//decimalFormat can format numbers in variety of ways
		String rounded = digit.format(number);
		
		System.out.println(rounded);

	}

}
